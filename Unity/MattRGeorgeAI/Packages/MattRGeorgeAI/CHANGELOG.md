# Changelog

Format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).
Project uses [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

To see pre-NPM hosted changelog look at: preNPMHosted_CHANGELOG.md.

## [2.0.0] - 9/30/21
### [Changed]
- Renamed package to match namespace name format.
- Moved samples traits to new `AITraits` and `PassiveTraits` namespaces and `SenseTrait` senses to a `PassiveTraits.Senses` namespace.
- Improved class inheritanc: `AIIndividual.AIBrainBase`, `.AIBrain`, `.Trait`, `.AITrait`, `.AIManagers.AIManager`, `.AIManagers.AIManagerDistPattern`, `Samples.Traits.AITraits.RandomWanderTrait`, `.MoveToSensedObjectTrait`, `Samples.Traits.PassiveTraits.SensesTrait`, `.Senses.Sense`, `Scent`, `HearingSense`, `SightSense`, and `SmellSense`.
- Moved `AIIndividual.AIManagers.AIFramePatternTypes` to its own file and new `.AIManagers.Enums` namespace.
- Updated samples assets.
### [Added]
- `AIIndividual.AIManagers.AIManagerDistPattern` now checks/logs the required value of `.players`.
### [Fixed]
- Fixed `Samples.Traits.PassiveTraits.MovementTrait` a null check and now disables debug gizmos when object is disabled.

## [1.1.0] - 9/26/21
### [Changed]
- `SightTrait` sample asset now only uses FOV instead of horizontal and vertical FOV.
- Updated samples assets.
- Now requires `MattRGeorge` package version 8.0.0 minnimum.

## [1.0.3] - 7/3/20
### [Added]
- Can now assign a GameObject that holds multiple Traits to be found by the AIBrainBase.
### [Changed]
- Updated sample assets.

## [1.0.1] - 7/1/20
### [Added]
- Ability to set objects to always be checked, with allowed tags and layers to Sense.
- MoveToSensedObjectTrait which is a sample AITrait that isn't intended to be a default trait.
### [Changed]
- RandomWanderTrait.SetWanderPoint() now returns bool so it can be returned directly in Performing().
### [Fixed]
- Bug where if no default AI trait was found no AITraits nor PassiveTraits would be added to AIBrain.
- Improved MovementTrait's destination detection.

## [1.0.0] - 6/30/20
### [Added]
- Samples from MattRGeorge package.
- AI managers from MattRGeorge package.
- AI brains and main abstract trait classes from MattRGeorge package.