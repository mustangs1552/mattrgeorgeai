﻿using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.AI.AIIndividual;
using MattRGeorge.Unity.Utilities.Static;
using MattRGeorge.AI.Samples.Traits.PassiveTraits;

namespace MattRGeorge.AI.Samples.Traits.AITraits
{
    /// <summary>
    /// Moves to randomly chosen sensed objects every so often following a cooldown.
    /// </summary>
    public class MoveToSensedObjectTrait : AITrait
    {
        [Tooltip("Cooldown after an object was reached or trait was canceled.")]
        [SerializeField] protected float cooldown = 5;

        /// <summary>
        /// Cooldown after an object was reached or trait was canceled.
        /// </summary>
        public virtual float Cooldown
        {
            get => cooldown;
            set => cooldown = (value < 0) ? 0 : value;
        }

        protected MovementTrait movementTrait = null;
        protected SensesTrait sensesTrait = null;
        protected bool readyForNextMove = true;
        protected Coroutine cooldownCoroutine = null;
        protected bool isMoving = false;

        /// <summary>
        /// Needs attention if required traits are present, ready to move or is currently moving.
        /// </summary>
        /// <returns>True when needs attention.</returns>
        public override bool Checking()
        {
            return movementTrait && sensesTrait && (readyForNextMove || isMoving);
        }

        /// <summary>
        /// Starts moving if not already moving.
        /// Still needs attention if still moving.
        /// </summary>
        /// <returns>True when still needs attention.</returns>
        public override bool Performing()
        {
            if (!isMoving) StartMoving();
            return isMoving;
        }
        /// <summary>
        /// Cancels the current movement and starts the cooldown.
        /// </summary>
        /// <returns>True if succesfully canceled.</returns>
        public override bool CancelPerform()
        {
            if (cooldownCoroutine == null) StartCooldown();
            if (movementTrait) movementTrait.CancelDestination();

            return true;
        }

        public override void PreTraitSetup()
        {
            
        }
        /// <summary>
        /// Gets required traits.
        /// </summary>
        public override void TraitSetup()
        {
            if (!Brain) return;

            movementTrait = Brain.GetTrait<MovementTrait>();
            if (!movementTrait) UnityLoggingUtility.LogMissingValue(GetType(), "movementTrait", gameObject);

            sensesTrait = Brain.GetTrait<SensesTrait>();
            if (!sensesTrait) UnityLoggingUtility.LogMissingValue(GetType(), "sensesTrait", gameObject);
        }
        public override void PostTraitSetup()
        {
            
        }

        /// <summary>
        /// Resets the trait and cancels any current movement.
        /// </summary>
        public override void TraitReset()
        {
            if (cooldownCoroutine != null) StopCoroutine(cooldownCoroutine);
            if (movementTrait) movementTrait.CancelDestination();
            readyForNextMove = true;
        }

        /// <summary>
        /// Find a sensed object and start moving towards it.
        /// </summary>
        protected virtual void StartMoving()
        {
            if (!movementTrait || !sensesTrait) return;

            List<Transform> sensedObjs = sensesTrait.CheckSenses();
            if (sensedObjs == null || sensedObjs.Count == 0)
            {
                StartCooldown();
                return;
            }

            movementTrait.SetDestination(sensedObjs[Random.Range(0, sensedObjs.Count)].position, OnMovementEnded);
            isMoving = true;
            readyForNextMove = false;
        }

        /// <summary>
        /// Start the cooldown timer.
        /// </summary>
        protected virtual void StartCooldown()
        {
            readyForNextMove = false;
            cooldownCoroutine = StartCoroutine(CoroutineUtility.DelayedMethodCoroutine(OnCoolddownEnded, Cooldown));
        }

        /// <summary>
        /// Cooldown timer is up.
        /// </summary>
        protected virtual void OnCoolddownEnded()
        {
            readyForNextMove = true;
        }
        /// <summary>
        /// Movement ended due to reaching target or some other reason.
        /// </summary>
        /// <param name="successful">Successfully reached target?</param>
        protected virtual void OnMovementEnded(bool successful)
        {
            isMoving = false;
            StartCooldown();
        }

        protected virtual void OnValidate()
        {
            Cooldown = cooldown;
        }
    }
}
