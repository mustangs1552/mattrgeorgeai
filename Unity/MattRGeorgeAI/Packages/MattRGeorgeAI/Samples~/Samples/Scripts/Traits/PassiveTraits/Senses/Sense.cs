﻿using UnityEngine;
using System.Collections.Generic;
using MattRGeorge.Unity.Utilities.Static;

namespace MattRGeorge.AI.Samples.Traits.PassiveTraits.Senses
{
    /// <summary>
    /// A base sense object for all senses that use the `SensesTrait` inherit from.
    /// </summary>
    public abstract class Sense : MonoBehaviour
    {
        [Tooltip("Objects to always check to see if it can be sensed.")]
        [SerializeField] protected List<Transform> alwaysCheckObjs = new List<Transform>();
        [Tooltip("Only these tags can be sensed by this sense (leave empty to allow any tag).")]
        [SerializeField] protected List<string> tagsToAllow = new List<string>();
        [Tooltip("Layer mask of layers that can be sensed by this sense.")]
        public LayerMask layersToAllow = ~0;

        /// <summary>
        /// Objects to always check to see if it can be sensed.
        /// Get returns duplicate.
        /// </summary>
        public virtual List<Transform> AlwaysCheckObjs
        {
            get => new List<Transform>(alwaysCheckObjs);
            set => alwaysCheckObjs = (value == null) ? new List<Transform>() : ListUtility.RemoveNullEntries(value);
        }
        /// <summary>
        /// Only these tags can be sensed by this sense (leave empty to allow any tag).
        /// Get returns duplicate.
        /// </summary>
        public virtual List<string> TagsToAllow
        {
            get => new List<string>(tagsToAllow);
            set => tagsToAllow = (value == null) ? new List<string>() : ListUtility.RemoveNullEntries(value);
        }

        public abstract List<Transform> CheckSense();
        public abstract List<Transform> CheckSense(List<Transform> objs);

        public abstract void SenseSetup();

        public abstract void SenseReset();

        /// <summary>
        /// Filter out invalid objects from the given list going by the allowed tags and layers.
        /// </summary>
        /// <param name="objs">The list to co through.</param>
        /// <returns>The list with the invalid tags and layers filtered out.</returns>
        protected virtual List<Transform> FilterOutInvalidObjectTagsAndLayers(List<Transform> objs)
        {
            if (objs == null || objs.Count == 0 || (layersToAllow == ~0 && tagsToAllow.Count == 0)) return objs;

            List<Transform> validObjs = objs;
            List<Transform> currObjs = new List<Transform>();

            if (layersToAllow != ~0)
            {
                foreach (Transform obj in validObjs)
                {
                    if (((1 << obj.gameObject.layer) & layersToAllow) == 0) continue;

                    currObjs.Add(obj);
                }
                validObjs = currObjs;
            }

            if (tagsToAllow.Count > 0)
            {
                currObjs = new List<Transform>();
                foreach (Transform obj in validObjs)
                {
                    if (!tagsToAllow.Contains(obj.gameObject.tag)) continue;

                    currObjs.Add(obj);
                }
                validObjs = currObjs;
            }

            return validObjs;
        }
        /// <summary>
        /// Add the objects that must always be checked to the given list.
        /// </summary>
        /// <param name="objs">The list to add the always check objects to.</param>
        /// <returns>The list with the always check objs included.</returns>
        protected virtual List<Transform> AddAlwaysCheckObjs(List<Transform> objs)
        {
            if (alwaysCheckObjs.Count == 0) return objs;

            List<Transform> validObjs = (objs == null) ? new List<Transform>() : objs;
            foreach (Transform obj in alwaysCheckObjs)
            {
                if (validObjs.Contains(obj) || obj.transform == gameObject.transform) continue;

                validObjs.Add(obj);
            }

            return validObjs;
        }
        /// <summary>
        /// Check that the list of objects contains allowed tags and layers and add the listed always check objects to the list.
        /// </summary>
        /// <param name="objs">The list to check and add needed objects.</param>
        /// <returns>The list with valid and needed objects.</returns>
        protected virtual List<Transform> CheckObjectsAndAddAlwaysCheckObjects(List<Transform> objs)
        {
            List<Transform> validObjs = FilterOutInvalidObjectTagsAndLayers(objs);
            validObjs = AddAlwaysCheckObjs(validObjs);
            return validObjs;
        }

        protected virtual void OnValidate()
        {
            AlwaysCheckObjs = alwaysCheckObjs;
            TagsToAllow = tagsToAllow;
        }
    }
}