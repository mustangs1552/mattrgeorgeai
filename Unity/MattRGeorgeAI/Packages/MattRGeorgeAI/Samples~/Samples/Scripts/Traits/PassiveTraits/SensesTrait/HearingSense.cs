﻿using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.Unity.Tools.ObjectPooling;

namespace MattRGeorge.AI.Samples.Traits
{
    /// <summary>
    /// This sense is the AI's ability to hear other objects.
    /// This looks for playing AudioSources.
    /// </summary>
    public class HearingSense : Sense
    {
        [Tooltip("The range that this AI can hear an audio source's range.")]
        [SerializeField] protected float range = 10;
        [Tooltip("The accuracy of this AI's ability to detect an audio source.")]
        [SerializeField, Range(0, 1)] protected float accuracy = .75f;
        [Tooltip("Enable debugging?")]
        public bool isDebug = false;
        [Tooltip("The frequency the sense test is ran.")]
        public float testFreq = 1;

        /// <summary>
        /// The range that this AI can hear an audio source's range.
        /// </summary>
        public float Range
        {
            get => range;
            set
            {
                range = (value < 0) ? 0 : value;
            }
        }
        /// <summary>
        /// The accuracy of this AI's ability to detect an audio source.
        /// </summary>
        public float Accuracy
        {
            get => accuracy;
            set
            {
                if (value < 0) accuracy = 0;
                else if (value > 1) accuracy = 1;
                else accuracy = value;
            }
        }

        protected float lastTest = 0;
        protected List<Transform> lastTestObjects = new List<Transform>();

        #region Methods
        public override List<Transform> CheckSense()
        {
            return CheckHearing(FindValidObjects());
        }
        public override List<Transform> CheckSense(List<Transform> objs)
        {
            return CheckHearing(objs);
        }

        public override void SenseReset()
        {
            
        }
        public override void SenseSetup()
        {
            
        }

        /// <summary>
        /// Calculate the accuracy for sensing the given audio source.
        /// </summary>
        /// <param name="audioSource">The audio scource to check the accuracy of.</param>
        /// <returns>A value between 0 and 1 representing the accuracy.</returns>
        protected virtual float CalcAccuracy(AudioSource audioSource)
        {
            if (!audioSource) return 0;

            float dist = Vector3.Distance(transform.position, audioSource.transform.position);
            float maxDist = Range + audioSource.maxDistance;
            if (dist >= maxDist) return 0;
            else return (Accuracy == 1) ? 1 : (((maxDist - dist) / maxDist) + Accuracy) / 2;
        }
        /// <summary>
        /// Check the given list of audio sources to see if this creature can hear any of them.
        /// </summary>
        /// <param name="audioSources">The audio sources to check.</param>
        /// <returns>The audio sources' transfoms that this creature hears.</returns>
        protected virtual List<Transform> CheckHearing(List<Transform> audioSources)
        {
            if (audioSources == null || audioSources.Count == 0) return new List<Transform>();

            List<Transform> sensedAudioSources = new List<Transform>();
            foreach (Transform audioSourceTrans in audioSources)
            {
                AudioSource audioSource = audioSourceTrans.GetComponent<AudioSource>();
                if (!audioSource || !audioSource.isPlaying || audioSource.mute) continue;

                float currAccuracy = CalcAccuracy(audioSource);
                if (currAccuracy <= 0) continue;
                else if (currAccuracy >= 1) sensedAudioSources.Add(audioSourceTrans);
                else
                {
                    float randNum = UnityEngine.Random.Range(0f, 100f) / 100f;
                    if (currAccuracy >= randNum) sensedAudioSources.Add(audioSourceTrans);
                }
            }

            return sensedAudioSources;
        }

        /// <summary>
        /// Find all the valid objects that this AI can sense.
        /// </summary>
        /// <returns>A list of valid sensable objects.</returns>
        protected virtual List<Transform> FindValidObjects()
        {
            if (!ObjectPoolManager.SINGLETON) return new List<Transform>();

            List<AudioSource> audioSources = ObjectPoolManager.SINGLETON.GetActiveObjectsComponents<AudioSource>();
            List<Transform> validObjs = new List<Transform>();
            audioSources.ForEach(x => validObjs.Add(x.transform));

            validObjs = CheckObjectsAndAddAlwaysCheckObjects(validObjs);

            return validObjs;
        }

        /// <summary>
        /// Runs a test that gets all sensed objects and shows the results via debug lines connecting those objects.
        /// Green: Did sense
        ///   Red: Did not sense
        /// </summary>
        protected virtual void SenseTest()
        {
            List<Transform> startingObjs = FindValidObjects();
            List<Transform> endingObjs = lastTestObjects;
            if (Time.time - lastTest >= testFreq)
            {
                endingObjs = CheckHearing(startingObjs);
                lastTestObjects = endingObjs;
                lastTest = Time.time;
            }

            foreach (Transform obj in startingObjs)
            {
                if (obj.gameObject.layer != (int)LayerMask.NameToLayer("Ignore Raycast")) Debug.DrawLine(transform.position, obj.position, (endingObjs.Contains(obj)) ? Color.green : Color.red);
            }
        }
        #endregion

        protected override void OnValidate()
        {
            base.OnValidate();

            Range = range;
            Accuracy = accuracy;
        }

        protected virtual void OnDrawGizmos()
        {
            if (isDebug)
            {
                SenseTest();

                Gizmos.color = Color.green;
                Gizmos.DrawWireSphere(transform.position, Range);
            }
        }
    }
}
