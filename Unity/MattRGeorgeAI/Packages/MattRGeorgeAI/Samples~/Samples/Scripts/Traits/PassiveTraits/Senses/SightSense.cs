﻿using UnityEngine;
using System.Collections.Generic;
using MattRGeorge.Unity.Utilities.Static;
using MattRGeorge.Unity.Tools.ObjectPooling;

namespace MattRGeorge.AI.Samples.Traits.PassiveTraits.Senses
{
    /// <summary>
    /// This sense is the AI's ability to see other objects.
    /// It uses a FOV, range, and LOS to check what is sensed.
    /// </summary>
    public class SightSense : Sense
    {
        [Tooltip("Enable debugging?")]
        public bool isDebug = false;
        [Tooltip("The distance this sense can detect an object.")]
        [SerializeField] protected float range = 10;
        [Tooltip("The FOV of this object.")]
        [SerializeField, Range(0, 359)] protected float fov = 180;

        /// <summary>
        /// The distance this sense can detect an object.
        /// </summary>
        public virtual float Range
        {
            get => range;
            set => range = (value < 0) ? 0 : value;
        }

        /// <summary>
        /// The FOV of this object.
        /// </summary>
        public virtual float FOV
        {
            get => fov;
            set
            {
                if (value < 0 || value >= 360) return;
                fov = value;
            }
        }

        /// <summary>
        /// Check to see what this sense currently sees.
        /// </summary>
        /// <returns>The transforms that this sense can see.</returns>
        public override List<Transform> CheckSense()
        {
            return CheckSight(FindValidObjects());
        }
        /// <summary>
        /// Check to see if this sense can see any of the given objects.
        /// </summary>
        /// <param name="objs">The objects to check.</param>
        /// <returns>The transforms that this sense can see.</returns>
        public override List<Transform> CheckSense(List<Transform> objs)
        {
            return CheckSight(objs);
        }

        public override void SenseSetup()
        {

        }

        public override void SenseReset()
        {

        }

        /// <summary>
        /// Get and filter through all the active objects for valid sensable objects.
        /// </summary>
        /// <returns>Valid sensable objects.</returns>
        protected virtual List<Transform> FindValidObjects()
        {
            if (!ObjectPoolManager.SINGLETON) return new List<Transform>();

            List<Transform> validObjs = new List<Transform>();
            List<Transform> currObjs = ObjectPoolManager.SINGLETON.AllTransforms;
            foreach (Transform obj in currObjs)
            {
                if (validObjs.Contains(obj) || obj.transform == gameObject.transform) continue;

                validObjs.Add(obj);
            }

            validObjs = CheckObjectsAndAddAlwaysCheckObjects(validObjs);

            return validObjs;
        }

        /// <summary>
        /// Check the list of valid objects to see which are actually sensed.
        /// </summary>
        /// <param name="validObjs">The list of valid objects.</param>
        /// <returns>The list of sensed objects.</returns>
        protected virtual List<Transform> CheckSight(List<Transform> validObjs)
        {
            if (validObjs == null || validObjs.Count == 0) return new List<Transform>();

            List<Transform> nullessValidObjs = ListUtility.RemoveNullEntries(validObjs);
            List<Transform> detectedObjs = new List<Transform>();
            RaycastHit hit = new RaycastHit();
            foreach (Transform obj in nullessValidObjs)
            {
                // Within range and FOV
                float objDist = Vector3.Distance(transform.position, obj.position);
                if (objDist <= range && TransformUtility.CheckFOV(transform, obj, fov))
                {
                    // LOS
                    hit = new RaycastHit();
                    if (Physics.Linecast(transform.position, obj.position, out hit))
                    {
                        if (hit.transform == obj) detectedObjs.Add(obj);
                    }
                }
            }

            return detectedObjs;
        }

        /// <summary>
        /// Runs a test that gets all sensed objects and shows the results via debug lines connecting those objects.
        /// Green: Did sense
        ///   Red: Did not sense
        /// </summary>
        protected virtual void SenseTest()
        {
            List<Transform> startingObjs = FindValidObjects();
            List<Transform> endingObjs = CheckSight(startingObjs);

            foreach (Transform obj in startingObjs)
            {
                if (obj.gameObject.layer != (int)LayerMask.NameToLayer("Ignore Raycast")) Debug.DrawLine(transform.position, obj.position, (endingObjs.Contains(obj)) ? Color.green : Color.red);
            }
        }

        protected override void OnValidate()
        {
            base.OnValidate();

            Range = range;
        }

        protected virtual void OnDrawGizmos()
        {
            if (isDebug)
            {
                SenseTest();

                Gizmos.color = Color.green;
                Gizmos.DrawWireSphere(transform.position, range);
            }
        }
    }
}