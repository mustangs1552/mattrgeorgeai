﻿using UnityEngine;

namespace MattRGeorge.AI.AIIndividual
{
    /// <summary>
    /// The AI Traits are the main Traits of the AI.
    /// These require attention from the AI because they cannot be performed alongside other AI Traits.
    /// They would typically use Passive Traits to get values and/or perform their actions.
    /// For example: Various AI Traits may need to use the Movement Passive Trait to move to a target to be able to perform their action.
    /// </summary>
    public abstract class AITrait : Trait
    {
        [Tooltip("If isDefault is true then priority will be ignored and this AI trait will only be ran when no other AI traits need attention.")]
        public bool isDefault = false;
        [Tooltip("The priority of this AI trait compared to other AI traits.")]
        [SerializeField] protected int priority = 10;

        /// <summary>
        /// This module's priority the AIBrain uses to determine which module can perform their needed actions.
        /// </summary>
        public virtual int CurrPriority
        {
            get => currPriority;
        }

        protected int currPriority = 0;

        /// <summary>
        /// Called by AIBrain before the abstract setups.
        /// </summary>
        public override void InitialTraitSetup(AIBrainBase brain)
        {
            base.InitialTraitSetup(brain);
            Setup();
        }

        /// <summary>
        /// Called from the AIBrain when it is trying to see if this Trait needs attention.
        /// </summary>
        /// <returns>True if this Trait does need attention.</returns>
        public abstract bool Checking();
        /// <summary>
        /// Called from the AIBrain when it has chosen this Trait to perform what it needs after Checking() returned true meaning this Trait needed attention.
        /// </summary>
        /// <returns>True when attention is still needed.</returns>
        public abstract bool Performing();
        /// <summary>
        /// Called from the AIBrain when it decides that this Trait no longer has priority.
        /// Cancel whatever needs to be canceled.
        /// AIBrain will re-call Checking() to make sure that this Trait still needs attention once this Trait has priority again.
        /// If not possible to be canceled then AIBrain will re-check the same Trait that needed attendtion until cancelation is possible.
        /// </summary>
        /// <returns>True if successfully canceled.</returns>
        public abstract bool CancelPerform();

        /// <summary>
        /// Initial setup.
        /// </summary>
        protected virtual void Setup()
        {
            currPriority = (priority >= 0) ? priority : 0;
        }
    }
}