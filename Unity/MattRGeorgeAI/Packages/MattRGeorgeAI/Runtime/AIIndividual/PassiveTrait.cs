﻿namespace MattRGeorge.AI.AIIndividual
{
    /// <summary>
    /// Passive Traits can perform their tasks alongside other Traits.
    /// There are generally two types of Passive Traits: Passive Traits and System Traits.
    /// Passive Traits work primarily on their own and their values can be used by other Traits.
    /// They cannot be told to do something by other Traits but, they can tell other Traits do do something.
    /// System Traits are the same as Passive Traits but, they need to be told to do something and they handle what needs to be done passively.
    /// For example: Movement would be considered a System Trait, it does nothing until it is told to move somewhere by another Trait and then it will passively move and check the progress.
    /// </summary>
    public abstract class PassiveTrait : Trait
    {
        /// <summary>
        /// This should replace Update() so the AI can be managed.
        /// Called from the AIBrain saying to update/perform this trait.
        /// </summary>
        public abstract void UpdateTrait();
    }
}