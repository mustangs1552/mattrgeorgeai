﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.Unity.Utilities.Static;
using MattRGeorge.Unity.Utilities.Enums;
using MattRGeorge.Unity.Tools.ObjectPooling;

namespace MattRGeorge.AI.AIIndividual
{
    /// <summary>
    /// The base of the AI brain.
    /// The base has all the traits stored as normal traits and sets them up as such.
    /// </summary>
    public abstract class AIBrainBase : MonoBehaviour, IObjectPoolEvents
    {
        [Tooltip("All the traits that belong to this AI.")]
        [SerializeField] protected List<Trait> traits = new List<Trait>();
        [Tooltip("Objects that are suppose to have other traits on them.")]
        [SerializeField] protected List<GameObject> objsWithTraits = new List<GameObject>();
        [Tooltip("Perform this AI's setup on start/awake?")]
        public UnityStartMethods autoSetupOn = UnityStartMethods.None;

        /// <summary>
        /// All the traits that belong to this AI.
        /// Get returns duplicate.
        /// </summary>
        protected virtual List<Trait> Traits
        {
            set => traits = (value == null) ? new List<Trait>() : ListUtility.RemoveNullEntries(value);
        }
        /// <summary>
        /// Objects that are suppose to have other traits on them.
        /// Get returns duplicate.
        /// </summary>
        protected virtual List<GameObject> ObjsWithTraits
        {
            set => objsWithTraits = (value == null) ? new List<GameObject>() : ListUtility.RemoveNullEntries(value);
        }

        protected Dictionary<Type, Trait> traitAccess = new Dictionary<Type, Trait>();
        protected bool setupRan = false;

        /// <summary>
        /// Perform everything this AI needs to do.
        /// </summary>
        public abstract void RunAIOnce();

        /// <summary>
        /// Reset the AI and its traits.
        /// </summary>
        public virtual void ResetAI()
        {
            ResetTraits();
            OnResetAI();
        }
        /// <summary>
        /// Reset the traits.
        /// </summary>
        public virtual void ResetTraits()
        {
            if (!setupRan || traits.Count > 0) return;

            foreach (Trait trait in traits) trait.TraitReset();
        }

        /// <summary>
        /// Get a trait that the AI brain is using.
        /// </summary>
        /// <param name="type">The class type of the desired trait.</param>
        /// <returns>The trait desired.</returns>
        public virtual Trait GetTrait(Type type)
        {
            if (type == null || traitAccess == null) return null;

            if (traitAccess.ContainsKey(type)) return traitAccess[type];

            return null;
        }
        /// <summary>
        /// Get a trait that the AI brain is using.
        /// </summary>
        /// <returns>The trait desired.</returns>
        public virtual T GetTrait<T>()
        {
            if (traitAccess != null && traitAccess.ContainsKey(typeof(T))) return (T)Convert.ChangeType(traitAccess[typeof(T)], typeof(T));

            return default;
        }

        /// <summary>
        /// Initial setup of traits.
        /// </summary>
        public virtual void SetupTraits()
        {
            if (setupRan) return;

            FindOtherTraits();

            InitialTraitSetup();
            RunOtherTraitSetups();

            OnSetupTraits();

            setupRan = true;
        }

        /// <summary>
        /// Reset the AI when potentially re-spawned by object pool system.
        /// </summary>
        public virtual void OnInstantiatedByObjectPool()
        {
            ResetAI();
        }
        public virtual void OnDestroyedByObjectPool()
        {

        }

        /// <summary>
        /// Called when this AI was reset.
        /// </summary>
        protected virtual void OnResetAI()
        {

        }

        /// <summary>
        /// Goes through all the objects with traits and checks for traits on them.
        /// </summary>
        protected virtual void FindOtherTraits()
        {
            if (objsWithTraits.Count == 0) return;

            Trait[] currTraitsFound = null;
            List<Trait> savedTraits = traits;
            foreach(GameObject obj in objsWithTraits)
            {
                currTraitsFound = obj.GetComponents<Trait>();
                if (currTraitsFound == null || currTraitsFound.Length == 0) continue;

                currTraitsFound.ToList().ForEach(x => savedTraits.Add(x));
            }
            Traits = savedTraits;
        }

        /// <summary>
        /// Initial setup of all the traits on this AI.
        /// </summary>
        protected virtual void InitialTraitSetup()
        {
            if (traits.Count == 0) return;

            if (traitAccess == null) traitAccess = new Dictionary<Type, Trait>();
            foreach (Trait trait in traits)
            {
                trait.InitialTraitSetup(this);
                trait.PreTraitSetup();
                traitAccess.Add(trait.GetType(), trait);
            }
        }
        /// <summary>
        /// Run all other setups on the traits on this AI.
        /// </summary>
        protected virtual void RunOtherTraitSetups()
        {
            if (traits.Count == 0) return;

            traits.ForEach(x => x.TraitSetup());
            traits.ForEach(x => x.PostTraitSetup());
        }
        /// <summary>
        /// Called when this AI has setup all its traits.
        /// </summary>
        protected virtual void OnSetupTraits()
        {

        }

        /// <summary>
        /// Check for any missing required values.
        /// </summary>
        protected virtual void CheckMissingValues()
        {
            if (traits.Count == 0) UnityLoggingUtility.LogMissingValue(GetType(), "traits", gameObject);
        }

        protected virtual void OnValidate()
        {
            Traits = traits;
            ObjsWithTraits = objsWithTraits;
        }

        protected virtual void Awake()
        {
            CheckMissingValues();

            if (autoSetupOn == UnityStartMethods.Awake) StartCoroutine(CoroutineUtility.DelayedMethodCoroutine(SetupTraits, .25f));
        }
        protected virtual void Start()
        {
            if (autoSetupOn == UnityStartMethods.Start) StartCoroutine(CoroutineUtility.DelayedMethodCoroutine(SetupTraits, .25f));
        }
    }
}
