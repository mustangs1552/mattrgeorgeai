﻿using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.Unity.Utilities.Static;

namespace MattRGeorge.AI.AIIndividual
{
    /// <summary>
    /// The main class for a creature.
    /// The main job of this class is to make decissions on what the creature will do.
    /// The various traits that this class manages are what "does" things.
    /// Uses the AITraits on this creature to determine what the creature does.
    /// The AITraits can't perform thier actions on at the same time so therefore, this class determines which ones can perform thier actions first.
    /// They know what they need to do.
    /// </summary>
    public class AIBrain : AIBrainBase
    {
        /// <summary>
        /// The default AI trait that this class is using.
        /// </summary>
        public virtual AITrait DefaultAITrait { get; protected set; }
        /// <summary>
        /// The current AI trait that this class chosen to do its thing.
        /// </summary>
        public virtual AITrait CurrAITrait { get; protected set; }

        /// <summary>
        /// The current AITraits known.
        /// </summary>
        protected virtual List<AITrait> AITraits
        {
            set => aiTraits = (value == null) ? new List<AITrait>() : ListUtility.RemoveNullEntries(value);
        }
        /// <summary>
        /// The current PassiveTraits known.
        /// </summary>
        protected virtual List<PassiveTrait> PassiveTraits
        {
            set => passiveTraits = (value == null) ? new List<PassiveTrait>() : ListUtility.RemoveNullEntries(value);
        }

        protected List<AITrait> aiTraits = new List<AITrait>();
        protected List<PassiveTrait> passiveTraits = new List<PassiveTrait>();
        protected int currAITraitI = 0;

        /// <summary>
        /// Perform everything this AI needs to do.
        /// </summary>
        public override void RunAIOnce()
        {
            AITraitLoop();
            PassiveTraitLoop();
        }

        /// <summary>
        /// Checks to see if this AI is performing an AI trait other than the default trait.
        /// </summary>
        /// <returns>True if it is performing an AI trait that isn't the default trait.</returns>
        public virtual bool IsPerformingAITrait()
        {
            return CurrAITrait && DefaultAITrait && CurrAITrait != DefaultAITrait;
        }
        /// <summary>
        /// Checks to see if it is performing the given trait.
        /// Can also check the opposite, if it is performing any other trait that isn't the default trait or the given trait.
        /// </summary>
        /// <param name="trait">The trait to check.</param>
        /// <param name="invertCheck">If true than check the opposite.</param>
        /// <returns>True if given trait is performing or the any other trait is performing when invertCheck is true.</returns>
        public virtual bool IsPerformingAITrait(AITrait trait, bool invertCheck = false)
        {
            if (!trait) return false;

            if (!invertCheck) return CurrAITrait && CurrAITrait == trait;
            else return IsPerformingAITrait() && CurrAITrait != trait;
        }

        /// <summary>
        /// Checks the current AITrait to see if it needs attention.
        /// If it does need attention and one is already being performed and this new AITrait has higher priority then it will cancel what its doing and this one will replace it.
        /// If it doesn't then the default trait will become active if it isn't already.
        /// </summary>
        protected virtual void CheckAITrait()
        {
            if (aiTraits.Count == 0 && !DefaultAITrait) return;

            // Check current AI trait in line to check
            if (aiTraits[currAITraitI].Checking())
            {
                if (!CurrAITrait)
                {
                    if (DefaultAITrait && CurrAITrait == DefaultAITrait)
                    {
                        if (CurrAITrait.CancelPerform()) CurrAITrait = aiTraits[currAITraitI];
                        else currAITraitI--;
                    }
                    else CurrAITrait = aiTraits[currAITraitI];
                }
                else if (CurrAITrait.CurrPriority < aiTraits[currAITraitI].CurrPriority)
                {
                    if (CurrAITrait.CancelPerform()) CurrAITrait = aiTraits[currAITraitI];
                    else currAITraitI--;
                }
            }
            else if (aiTraits[currAITraitI] == CurrAITrait) CurrAITrait = null;

            // Increment to next AI trait in line to be checked
            currAITraitI++;
            if (currAITraitI >= aiTraits.Count) currAITraitI = 0;

            // If no AI trait was selected this pass then check the default AI trait
            if (!CurrAITrait && DefaultAITrait)
            {
                if (DefaultAITrait.Checking()) CurrAITrait = DefaultAITrait;
            }
        }
        /// <summary>
        /// Performs the current AITrait's actions if one is active.
        /// </summary>
        protected virtual void PerformAITrait()
        {
            if (!CurrAITrait) return;

            if (!CurrAITrait.Performing()) CurrAITrait = null;
        }
        /// <summary>
        /// The main loop that checks the AITraits and performs the AITrait that currently needs the most attention.
        /// </summary>
        protected virtual void AITraitLoop()
        {
            CheckAITrait();
            PerformAITrait();
        }

        /// <summary>
        /// Run all the passive traits.
        /// </summary>
        protected virtual void PassiveTraitLoop()
        {
            if (passiveTraits.Count == 0) return;

            foreach (PassiveTrait trait in passiveTraits) trait.UpdateTrait();
        }

        /// <summary>
        /// Find all the passive and AI traits from the base classe's traits and populate the lists for each.
        /// </summary>
        protected virtual void FindTraits()
        {
            if (traits.Count == 0) return;

            PassiveTraits = new List<PassiveTrait>();
            AITraits = new List<AITrait>();
            PassiveTrait currPassiveTrait = null;
            AITrait currAITrait = null;
            foreach (Trait trait in traits)
            {
                currPassiveTrait = trait as PassiveTrait;
                if (currPassiveTrait)
                {
                    passiveTraits.Add(currPassiveTrait);
                    continue;
                }

                currAITrait = trait as AITrait;
                if (currAITrait)
                {
                    if (currAITrait.isDefault)
                    {
                        if (!DefaultAITrait) DefaultAITrait = currAITrait;
                        else Debug.LogWarning("There can only be one default trait!");
                    }
                    else aiTraits.Add(currAITrait);
                }
            }
        }
        /// <summary>
        /// Checks to see if any AITraits' priorities are the same.
        /// For intended behaviour, priorities should be different.
        /// Returns after the first match.
        /// </summary>
        /// <returns>True if two AITraits' priorities match.</returns>
        protected virtual bool CheckAITraitsPriorities()
        {
            if (aiTraits.Count == 0) return false;

            foreach (AITrait traitOne in aiTraits)
            {
                foreach (AITrait traitTwo in aiTraits)
                {
                    if (traitTwo != traitOne && traitTwo.CurrPriority == traitOne.CurrPriority)
                    {
                        Debug.LogWarning(traitTwo.GetType() + " and " + traitOne.GetType() + " have the same priority! Behaviour may not be as intended.");
                        return true;
                    }
                }
            }

            return false;
        }
        /// <summary>
        /// Setup the traits into passive and AI traits.
        /// </summary>
        protected override void OnSetupTraits()
        {
            FindTraits();
            CheckAITraitsPriorities();
        }
    }
}