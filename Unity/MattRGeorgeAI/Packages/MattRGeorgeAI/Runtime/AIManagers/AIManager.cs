﻿using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.Unity.Utilities.Static;
using MattRGeorge.Unity.Utilities.Enums;
using MattRGeorge.Unity.Tools.ObjectPooling;
using MattRGeorge.AI.AIIndividual;

namespace MattRGeorge.AI.AIManagers
{
    /// <summary>
    /// An AI manager that simply goes through the list of AI and tells them to perform their needed actions.
    /// </summary>
    public class AIManager : MonoBehaviour
    {
        [Tooltip("The frequency that the AI are performed.")]
        [SerializeField] protected float frequency = 1;
        [Tooltip("Start on start/awake?")]
        public UnityStartMethods startOn = UnityStartMethods.None;

        /// <summary>
        /// The frequency that the AI are performed.
        /// </summary>
        public virtual float Frequency
        {
            get => frequency;
            set
            {
                frequency = (value < 0) ? 0 : value;
                ResetLoop();
            }
        }

        /// <summary>
        /// Is the looping currently active?
        /// </summary>
        public virtual bool LoopActive { get; set; } = false;

        protected Coroutine loopCoroutine = null;

        /// <summary>
        /// Go through all the AI and perform them once.
        /// </summary>
        /// <returns>True if can continue looping.</returns>
        public virtual bool DoPass()
        {
            if (!ObjectPoolManager.SINGLETON || !LoopActive) return false;

            List<AIBrain> brains = ObjectPoolManager.SINGLETON.GetActiveObjectsComponents<AIBrain>();
            foreach (AIBrain brain in brains)
            {
                if (brain.gameObject.activeInHierarchy) brain.RunAIOnce();
            }

            return true;
        }

        /// <summary>
        /// Stop and start the AI loop using the current frequency.
        /// </summary>
        public virtual void ResetLoop()
        {
            if (loopCoroutine != null) StopCoroutine(loopCoroutine);

            LoopActive = true;
            loopCoroutine = StartCoroutine(CoroutineUtility.WhileCoroutine(DoPass, null, Frequency, Frequency));
        }

        protected virtual void OnValidate()
        {
            Frequency = frequency;
        }

        protected virtual void Awake()
        {
            if (startOn == UnityStartMethods.Awake) ResetLoop();
        }
        protected virtual void Start()
        {
            if (startOn == UnityStartMethods.Start) ResetLoop();
        }
    }
}