﻿using UnityEngine;
using System.Collections.Generic;
using MattRGeorge.Unity.Utilities.Static;
using MattRGeorge.Unity.Tools.ObjectPooling;
using MattRGeorge.AI.AIIndividual;
using MattRGeorge.AI.AIManagers.Enums;

namespace MattRGeorge.AI.AIManagers
{
    /// <summary>
    /// Manages all the AI in the scene by different distances from the player.
    /// Can set a pattern that the manager will follow and run the AI that falls in that current distance for the current pass.
    /// Can also set a max amount of AI to run for that pass.
    /// </summary>
    public class AIManagerDistPattern : AIManager
    {
        [Tooltip("Max amount of AI for medium and far AI each frame.")]
        [SerializeField] protected int maxDistantAIPerPass = 5;
        [Tooltip("Max distance for nearby AI.")]
        [SerializeField] protected float nearbyAIMaxDist = 20;
        [Tooltip("Max distance for medium AI.")]
        [SerializeField] protected float mediumAIMaxDist = 40;
        [Tooltip("AI pattern order. Each is ran in one frame and `frequency` is between each then wraps around to the first.")]
        [SerializeField] protected List<AIFramePatternTypes> aiPattern = new List<AIFramePatternTypes>() { AIFramePatternTypes.Nearby, AIFramePatternTypes.Medium, AIFramePatternTypes.Far };
        [Tooltip("The players that this manager is using to determine which AI gets ran by distance.")]
        [SerializeField] protected List<Transform> players = new List<Transform>();

        /// <summary>
        /// Max amount of AI for medium and far AI each frame.
        /// </summary>
        public virtual int MaxDistantAIPerPass
        {
            get => maxDistantAIPerPass;
            set => maxDistantAIPerPass = (value < 0) ? 0 : value;
        }
        /// <summary>
        /// Max distance for nearby AI.
        /// </summary>
        public virtual float NearbyAIMaxDist
        {
            get => nearbyAIMaxDist;
            set => nearbyAIMaxDist = (value < 0) ? 0 : value;
        }
        /// <summary>
        /// Max distance for medium AI.
        /// </summary>
        public virtual float MediumAIMaxDist
        {
            get => mediumAIMaxDist;
            set => mediumAIMaxDist = (value < 0) ? 0 : value;
        }
        /// <summary>
        /// AI pattern order.
        /// Each is ran in one frame and `frequency` is between each then wraps around to the first.
        /// </summary>
        public virtual List<AIFramePatternTypes> AIPattern
        {
            get => new List<AIFramePatternTypes>(aiPattern);
            set
            {
                aiPattern = (value == null) ? new List<AIFramePatternTypes>() : value;
                aiPassPatternI = 0;
            }
        }

        /// <summary>
        /// The players that this manager is using to determine which AI gets ran by distance.
        /// </summary>
        public virtual List<Transform> Players
        {
            get => new List<Transform>(players);
            set => players = (value == null) ? new List<Transform>() : ListUtility.RemoveNullEntries(value);
        }

        protected int aiPassPatternI = 0;
        protected int currLoopNum = 0;
        protected Dictionary<int, int> aiLoopNumbers = new Dictionary<int, int>();
        protected float dist = 0;
        protected float closest = -1;
        protected int leastRecentRanAI = -1;
        protected bool inRange = false;
        protected int mostRecentI = 0;
        protected int mostRecent = 0;

        /// <summary>
        /// Main AI loop.
        /// Determine if nearby, medium, or far AI should be performed this pass using AI pattern.
        /// </summary>
        /// <returns>True if can continue looping.</returns>
        public override bool DoPass()
        {
            if (aiPattern == null || aiPattern.Count > 0 || !LoopActive) return false;

            foreach(Transform player in players)
            {
                if (aiPattern[aiPassPatternI] == AIFramePatternTypes.Nearby) DoNearbyAILoop(player);
                else if (aiPattern[aiPassPatternI] == AIFramePatternTypes.Medium) DoDistantAILoop(player, false);
                else if (aiPattern[aiPassPatternI] == AIFramePatternTypes.Far) DoDistantAILoop(player, true);
            }

            aiPassPatternI++;
            if (aiPassPatternI >= aiPattern.Count) aiPassPatternI = 0;

            currLoopNum++;
            return true;
        }

        /// <summary>
        /// Performs all AI that are within the nearby range of the player.
        /// </summary>
        protected virtual void DoNearbyAILoop(Transform player)
        {
            if (!ObjectPoolManager.SINGLETON || !player) return;

            List<AIBrain> objectManagerObjs = ObjectPoolManager.SINGLETON.GetActiveObjectsComponents<AIBrain>();
            closest = -1;
            AIBrain currAIBrain = null;
            foreach (AIBrain aiBrain in objectManagerObjs)
            {
                currAIBrain = aiBrain.GetComponent<AIBrain>();
                if (currAIBrain != null)
                {
                    if (!aiLoopNumbers.ContainsKey(currAIBrain.GetInstanceID())) aiLoopNumbers.Add(currAIBrain.GetInstanceID(), -1);
                    else if (aiLoopNumbers[currAIBrain.GetInstanceID()] == currLoopNum) continue;

                    dist = Vector3.Distance(player.position, aiBrain.transform.position);
                    if (closest == -1 || closest > dist) closest = dist;
                    if (dist <= nearbyAIMaxDist)
                    {
                        currAIBrain.RunAIOnce();
                        aiLoopNumbers[currAIBrain.GetInstanceID()] = currLoopNum;
                    }
                }
            }
        }
        /// <summary>
        /// Performs up to max distant AI that are within medium distance of player if doFar is false else will perform up to max distant AI for any AI beyond medium range.
        /// </summary>
        /// <param name="doFar">Target AI in the far range instead of medium.</param>
        protected virtual void DoDistantAILoop(Transform player, bool doFar)
        {
            if (!ObjectPoolManager.SINGLETON || !player) return;

            List<AIBrain> objectManagerObjs = ObjectPoolManager.SINGLETON.GetActiveObjectsComponents<AIBrain>();
            List<AIBrain> topChosen = new List<AIBrain>();
            leastRecentRanAI = -1;
            AIBrain currAIBrain = null;
            foreach (AIBrain aiBrain in objectManagerObjs)
            {
                currAIBrain = aiBrain.GetComponent<AIBrain>();
                if (currAIBrain != null)
                {
                    if (!aiLoopNumbers.ContainsKey(currAIBrain.GetInstanceID())) aiLoopNumbers.Add(currAIBrain.GetInstanceID(), -1);
                    else if (aiLoopNumbers[currAIBrain.GetInstanceID()] == currLoopNum) continue;

                    dist = Vector3.Distance(player.position, aiBrain.transform.position);
                    inRange = dist > mediumAIMaxDist;
                    if (!doFar) inRange = dist > nearbyAIMaxDist && dist <= mediumAIMaxDist;

                    if (inRange)
                    {
                        // List not full yet
                        if (topChosen.Count < maxDistantAIPerPass)
                        {
                            topChosen.Add(currAIBrain);
                            if (aiLoopNumbers[currAIBrain.GetInstanceID()] == -1 || leastRecentRanAI > aiLoopNumbers[currAIBrain.GetInstanceID()]) leastRecentRanAI = aiLoopNumbers[currAIBrain.GetInstanceID()];
                        }
                        // List full, replace most recent in list with current if current isn't as recent
                        else if (leastRecentRanAI >= aiLoopNumbers[currAIBrain.GetInstanceID()])
                        {
                            mostRecentI = 0;
                            mostRecent = -1;
                            for (int i = 0; i < topChosen.Count; i++)
                            {
                                if (mostRecent == -1 || mostRecent < aiLoopNumbers[topChosen[i].GetInstanceID()])
                                {
                                    mostRecentI = i;
                                    mostRecent = aiLoopNumbers[topChosen[i].GetInstanceID()];
                                }
                            }

                            topChosen.RemoveAt(mostRecentI);
                            topChosen.Add(currAIBrain);
                            leastRecentRanAI = aiLoopNumbers[currAIBrain.GetInstanceID()];
                        }
                    }
                }
            }

            foreach (AIBrain chosen in topChosen)
            {
                chosen.RunAIOnce();
                aiLoopNumbers[chosen.GetInstanceID()] = currLoopNum;
            }
        }

        /// <summary>
        /// Check for missing required values.
        /// </summary>
        protected virtual void CheckMissingValues()
        {
            if (players.Count == 0) UnityLoggingUtility.LogMissingValue(typeof(AIManagerDistPattern), "players", gameObject);
        }

        protected override void OnValidate()
        {
            base.OnValidate();

            MaxDistantAIPerPass = maxDistantAIPerPass;
            NearbyAIMaxDist = nearbyAIMaxDist;
            MediumAIMaxDist = mediumAIMaxDist;
            Players = players;
        }

        protected override void Awake()
        {
            CheckMissingValues();

            base.Awake();
        }
    }
}