﻿namespace MattRGeorge.AI.AIManagers.Enums
{
    /// <summary>
    /// Frame pattern distance types for `AIManagerDistPattern` AI Manager.
    /// </summary>
    public enum AIFramePatternTypes
    {
        Nearby,
        Medium,
        Far
    }
}
